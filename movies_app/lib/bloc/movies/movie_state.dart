import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movie_review.dart';
import 'package:movies_app/repos/movie_repo.dart';

abstract class MovieState{}

class MoviesListIsLoading extends MovieState{}

class MoviesListFailedToLoad extends MovieState{}


class MoviesListIs extends MovieState{
  final List<Movie> movies;
  MoviesListIs(this.movies);
}

class MovieIs extends MovieState{
  final Movie movie;
  MovieIs(this.movie);
}

class MovieDetailsLoading extends MovieState{
}