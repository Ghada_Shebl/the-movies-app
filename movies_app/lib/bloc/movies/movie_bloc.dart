import 'dart:collection';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movies_app/bloc/movies/movie_event.dart';
import 'package:movies_app/bloc/movies/movie_state.dart';
import 'package:movies_app/bloc/reviews/review_event.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movies_tab.dart';
import 'package:movies_app/repos/movie_repo.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/resources/strings.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  Map<int,Movie> openedMovies = HashMap();
  MovieRepo movieRepo = MovieRepo();
  List <MoviesTab> movieTabs = [
    MoviesTab(tabTitle: AppStrings.upcoming, apiLink: AppConstants.upcomingMoviesAPI, movies: List.empty(growable: true)),
    MoviesTab(tabTitle: AppStrings.popular, apiLink: AppConstants.popularMoviesAPI, movies: List.empty(growable: true)),
    MoviesTab(tabTitle: AppStrings.topRated, apiLink: AppConstants.topRatedMoviesAPI, movies: List.empty(growable: true)),
  ];

  String getTabTitle(int index){
    return movieTabs[index].tabTitle?.toString()??"";
  }


  MovieBloc() : super(MoviesListIsLoading()) {
    on<MoviesListLaunched>(_getMovies);
    on<DetailsPageLaunched>(_getMovieDetails);
  }

  Future <void> _getMovies(MoviesListLaunched event, Emitter emit)async{
    emit(MoviesListIsLoading());
    try {
      if (movieTabs[event.index].movies!.isEmpty){
        movieTabs[event.index].movies = await movieRepo.fetchMoviesFromAPI(movieTabs[event.index].apiLink);
      }
      emit(MoviesListIs(movieTabs[event.index].movies ?? []));
    } catch (error) {
      emit(MoviesListFailedToLoad());
    }
  }

  Future<void> _getMovieDetails(DetailsPageLaunched event, Emitter emit) async{
    emit(MovieDetailsLoading());
    try {
      if (!openedMovies.containsKey(event.movieId)){
        Movie movie =  await movieRepo.fetchMovieDetailsFromAPI(event.movieId);
        openedMovies.putIfAbsent(event.movieId, () => movie);
      }
      emit(MovieIs(openedMovies[event.movieId]??Movie()));
    } catch (error) {
      emit(MoviesListFailedToLoad());
    }
  }

  Future<void> _getMovieReviews(ReviewsListRequested event, Emitter emit) async{
    emit(MovieDetailsLoading());
    try {
      if (!openedMovies.containsKey(event.movieId)){
        Movie movie =  await movieRepo.fetchMovieDetailsFromAPI(event.movieId);
        openedMovies.putIfAbsent(event.movieId, () => movie);
      }
      emit(MovieIs(openedMovies[event.movieId]??Movie()));
    } catch (error) {
      emit(MoviesListFailedToLoad());
    }
  }
}