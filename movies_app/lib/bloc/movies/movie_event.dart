abstract class MovieEvent{}

class MoviesListLaunched extends MovieEvent {
  final int index;
  MoviesListLaunched(this.index);
}

class DetailsPageLaunched extends MovieEvent{
  final int movieId;
  DetailsPageLaunched(this.movieId);
}