abstract class ReviewEvent{}

class ReviewsListRequested extends ReviewEvent{
  final int movieId;
  ReviewsListRequested(this.movieId);
}