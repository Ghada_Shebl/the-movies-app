import 'package:movies_app/models/movie_review.dart';

abstract class ReviewState{}

class ReviewsListIs extends ReviewState{
  List <MovieReview> reviews;
  ReviewsListIs(this.reviews);
}

class ReviewsListIsLoading extends ReviewState{
}

class ReviewsListFailedToLoad extends ReviewState{
}

class ReviewsListIsEmpty extends ReviewState{
}