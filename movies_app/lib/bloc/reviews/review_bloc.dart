import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movies_app/bloc/reviews/review_event.dart';
import 'package:movies_app/bloc/reviews/review_state.dart';
import 'package:movies_app/models/movie_review.dart';
import 'package:movies_app/repos/review_repo.dart';


class ReviewBloc extends Bloc<ReviewEvent, ReviewState> {
  ReviewRepo reviewRepo = ReviewRepo();

  ReviewBloc() : super(ReviewsListIsLoading()) {
    on<ReviewsListRequested>(_getMovieReviews);
  }

  Future<void> _getMovieReviews(ReviewsListRequested event, Emitter emit) async{
    emit(ReviewsListIsLoading());
    try {
      List <MovieReview> list = await reviewRepo.fetchReviewsFromAPI(event.movieId)??List.empty();
      if (list.isEmpty){
        emit(ReviewsListIsEmpty());
        return;
      }
      emit(ReviewsListIs(list));
    } catch (error) {
      print ("ERRR: "+error.toString());
      emit(ReviewsListFailedToLoad());
    }
  }
}