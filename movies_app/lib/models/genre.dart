import 'package:json_annotation/json_annotation.dart';
import 'package:movies_app/resources/strings.dart';
part 'genre.g.dart';

@JsonSerializable()
class Genre{
  int id;
  String name;

  Genre({
    required this.id,
    required this.name,
  });

  factory Genre.fromJson(Map<String,dynamic> data) => _$GenreFromJson(data);

  Map<String,dynamic> toJson() => _$GenreToJson(this);
}