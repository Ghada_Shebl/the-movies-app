// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieReview _$MovieReviewFromJson(Map<String, dynamic> json) => MovieReview(
      content: json['content'] as String,
      createdAt: json['created_at'] as String,
      author: Author.fromJson(json['author_details'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MovieReviewToJson(MovieReview instance) =>
    <String, dynamic>{
      'content': instance.content,
      'created_at': instance.createdAt,
      'author_details': instance.author,
    };
