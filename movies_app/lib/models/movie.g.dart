// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Movie _$MovieFromJson(Map<String, dynamic> json) => Movie(
      title: json['title'] as String? ?? "",
      posterPath: json['poster_path'] as String? ?? "",
      overview: json['overview'] as String? ?? "",
      releaseDate: json['release_date'] as String? ?? "",
      rating: (json['vote_average'] as num?)?.toDouble() ?? 0,
      id: json['id'] as int? ?? 0,
      genres: (json['genres'] as List<dynamic>?)
          ?.map((e) => Genre.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MovieToJson(Movie instance) => <String, dynamic>{
      'title': instance.title,
      'poster_path': instance.posterPath,
      'overview': instance.overview,
      'release_date': instance.releaseDate,
      'vote_average': instance.rating,
      'id': instance.id,
      'genres': instance.genres,
    };
