import 'package:json_annotation/json_annotation.dart';
import 'package:movies_app/resources/strings.dart';
part 'author.g.dart';

@JsonSerializable()
class Author{
  String name;
  @JsonKey(name:'avatar_path')
  String? avatarPath;
  double? rating;

  Author({
    required this.name,
    this.avatarPath,
    this.rating
  });

  factory Author.fromJson(Map<String,dynamic> data) => _$AuthorFromJson(data);

  Map<String,dynamic> toJson() => _$AuthorToJson(this);
}