import 'package:json_annotation/json_annotation.dart';
import 'package:movies_app/models/genre.dart';
import 'package:movies_app/resources/strings.dart';
part 'movie.g.dart';

@JsonSerializable()
class Movie {

  String title;
  @JsonKey(name:'poster_path')
  String posterPath;
  String overview;
  @JsonKey(name:'release_date')
  String releaseDate;
  @JsonKey(name:'vote_average')
  double rating;
  int id;
  List<Genre>?genres;

  Movie({
    this.title = "",
    this.posterPath = "",
    this.overview = "",
    this.releaseDate = "",
    this.rating = 0,
    this.id = 0,
    this.genres
  });

  factory Movie.fromJson(Map<String,dynamic> data) => _$MovieFromJson(data);

  Map<String,dynamic> toJson() => _$MovieToJson(this);

}
