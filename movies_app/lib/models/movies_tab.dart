import 'package:movies_app/models/movie.dart';
import 'package:movies_app/resources/constants.dart';

class MoviesTab{
  String apiLink;
  String? tabTitle;
  List<Movie>? movies;

  MoviesTab({
    required this.apiLink,
    required this.tabTitle,
    required this.movies
  });

}