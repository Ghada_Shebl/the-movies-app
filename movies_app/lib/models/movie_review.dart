import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:movies_app/models/author.dart';
import 'package:movies_app/resources/constants.dart';
part 'movie_review.g.dart';

@JsonSerializable()
class MovieReview{
  String content;
  @JsonKey(name:'created_at')
  String createdAt;
  @JsonKey(name:'author_details')
  Author author;

  MovieReview({
    required this.content,
    required this.createdAt,
    required this.author
  });

  factory MovieReview.fromJson(Map<String,dynamic> data) => _$MovieReviewFromJson(data);

  Map<String,dynamic> toJson() => _$MovieReviewToJson(this);

}