import 'dart:convert';
import 'package:movies_app/models/movie.dart';
import 'package:dio/dio.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/resources/strings.dart';


class MovieRepo{
  final Dio _dio = Dio();

  Future<List<Movie>?> fetchMoviesFromAPI(String apiLink) async {
    final response = await _dio.get("${AppConstants.moviesBaseURL}$apiLink${AppConstants.apiKey}");
    if (response.statusCode == 200){
      final moviesMap = response.data[AppConstants.results];
      List temp =  moviesMap.map((i) => Movie.fromJson(i)).toList();
      return List<Movie>.from(temp);
    } else {
      throw Exception(AppStrings.failedToLoadMoviesException);
    }
  }

  Future<Movie> fetchMovieDetailsFromAPI(int movieId) async {
    final response = await _dio.get("${AppConstants.moviesBaseURL}$movieId${AppConstants.apiKey}");
    if (response.statusCode == 200) {
      Movie tempMovie = Movie.fromJson(response.data);
      return tempMovie;
    } else {
      throw Exception(AppStrings.failedToLoadMoviesException);
    }
  }
}