import 'dart:convert';
import 'dart:collection';
import 'package:movies_app/models/movie.dart';
import 'package:dio/dio.dart';
import 'package:movies_app/models/movie_review.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/resources/strings.dart';


class ReviewRepo{
  final Dio _dio = Dio();

  Future<List<MovieReview>?> fetchReviewsFromAPI(int movieId) async {
    final response = await _dio.get("${AppConstants.moviesBaseURL}$movieId${AppConstants.reviewsAPI}${AppConstants.apiKey}");
    print ("link: ${AppConstants.moviesBaseURL}$movieId${AppConstants.reviewsAPI}${AppConstants.apiKey}");
    if (response.statusCode == 200){
      final reviewsMap = response.data[AppConstants.results];
      List fetchedReviewsList =  reviewsMap.map((i) => MovieReview.fromJson(i)).toList();
      return List<MovieReview>.from(fetchedReviewsList);
    } else {
      throw Exception(AppStrings.failedToLoadMoviesException);
    }
  }
}