import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:movies_app/my_app.dart';

GetIt getIt = GetIt.instance;
void main() {
  runApp(const MyApp());
}
