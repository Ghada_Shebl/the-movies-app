import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movies_app/bloc/movies/movie_bloc.dart';
import 'package:movies_app/bloc/movies/movie_event.dart';
import 'package:movies_app/bloc/movies/movie_state.dart';
import 'package:movies_app/bloc/reviews/review_bloc.dart';
import 'package:movies_app/bloc/reviews/review_event.dart';
import 'package:movies_app/bloc/reviews/review_state.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movie_review.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/resources/strings.dart';
import 'package:movies_app/views/full_movie_review_dialog.dart';

class MoviesDetails extends StatefulWidget {
  final int movieId;
  const MoviesDetails({Key? key, required this.movieId}) : super(key: key);

  @override
  State<MoviesDetails> createState() => _MoviesDetailsState();
}

class _MoviesDetailsState extends State<MoviesDetails> {
  void _getData() {
    context.read<MovieBloc>().add(DetailsPageLaunched(widget.movieId));
    context.read<ReviewBloc>().add(ReviewsListRequested(widget.movieId));
  }

  @override
  void initState() {
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(AppStrings.movieDetails),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              BlocBuilder<MovieBloc, MovieState>(
                builder: (context, state) {
                  if (state is MovieDetailsLoading) {
                    return  Container(
                      margin: const EdgeInsets.only(top: 50),
                      child: const Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  if (state is MovieIs) {
                    return _buildDetailsSection(state.movie);
                  }
                  return Container();
                },
              ),
              BlocBuilder<ReviewBloc, ReviewState>(builder: (context, state) {
                if (state is ReviewsListIs) {
                  return _buildReviewsSection(state.reviews);
                }
                return Container();
              })
            ],
          ),
        ));
  }

  Widget _buildDetailsSection(Movie movie){
    return Column(
      children: [
        CachedNetworkImage(
          imageUrl: AppConstants.posterPathBaseURL +
              movie.posterPath,
          width: double.infinity,
          height: 300,
          fit: BoxFit.cover,
        ),
        Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(movie.title,
                      style: const TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold)),
                ),
                SizedBox(
                  height: 60,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    children: _buildGenresSection(movie),
                  ),
                ),
                RatingBarIndicator(
                    rating: movie.rating % 5,
                    itemSize: 20,
                    itemBuilder: (context, index) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    )),
                Container(
                  margin:
                  const EdgeInsets.only(bottom: 10, top: 10),
                  child: Text(
                      "${AppStrings.releaseDate} ${movie.releaseDate}"),
                ),
                const Divider(),
                Container(
                  margin:
                  const EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    AppStrings.plot,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17),
                  ),
                ),
                Container(
                  margin:
                  const EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    movie.overview,
                    style: const TextStyle(
                        fontSize: 15, height: 1.5),
                  ),
                ),
              ],
            ))
      ],
    );
  }

  Widget _buildReviewsSection(List <MovieReview> reviewsList) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Divider(),
          const SizedBox(
            height: 10,
          ),
          Text(
            AppStrings.reviews,
            style:
            const TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
          ),
          const SizedBox(
            height: 20,
          ),
          ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: _buildReviewsItems(reviewsList),
          )
        ],
      ),
    );
  }

  List<Widget> _buildGenresSection(Movie movie) {
    List<Widget> genresWidgets = List.empty(growable: true);
    for (int i = 0; i < movie.genres!.length; i++) {
      genresWidgets.add(Container(
        margin: const EdgeInsets.only(right: 10, top: 10, bottom: 10),
        color: Colors.grey.withOpacity(0.1),
        padding: const EdgeInsets.all(10),
        child: Text(
          movie.genres![i].name,
          style: const TextStyle(color: Colors.black87),
        ),
      ));
    }
    return genresWidgets;
  }

  List<Widget> _buildReviewsItems(List <MovieReview> reviewsList) {
    List<Widget> reviewsItemsList = List.empty(growable: true);
    for (int i = 0; i < reviewsList.length; i++) {
      reviewsItemsList.add(Container(
        margin: const EdgeInsets.only(bottom: 10),
        width: double.infinity,
        color: Colors.grey.withOpacity(0.1),
        child: TextButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) =>
                  FullMovieReviewDialog(movieReview: reviewsList[i]),
            );
          },
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: _getAuthorAvatarLink(reviewsList[i].author.avatarPath),
                        width: 30,
                        height: 30,
                        errorWidget: (context, url, error) => Container(
                          color: Colors.blueGrey,
                          child: const Icon(
                            Icons.person,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      reviewsList[i].author.name.isEmpty
                          ? AppStrings.unknown
                          : reviewsList[i].author.name,
                      style: const TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                  ],
                ),
                const SizedBox(height: 5),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RatingBarIndicator(
                        rating: reviewsList[i].author.rating??0% 5,
                        itemSize: 15,
                        itemBuilder: (context, index) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            )),
                    Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: Text(
                        reviewsList[i].createdAt,
                        style: const TextStyle(
                            fontSize: 13, color: Colors.black45),
                      ),
                    )
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Text(
                    reviewsList[i].content,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: const TextStyle(
                        fontSize: 15, height: 1.4, color: Colors.black87),
                  ),
                )
              ],
            ),
          ),
        ),
      ));
    }
    return reviewsItemsList;
  }

  String _getAuthorAvatarLink(String?avatarPath){
    String link = avatarPath == null? "":AppConstants.posterPathBaseURL + avatarPath;
    return link;
  }
}
