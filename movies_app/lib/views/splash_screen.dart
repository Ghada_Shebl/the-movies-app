import 'package:flutter/material.dart';
import 'package:movies_app/resources/colors.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/resources/strings.dart';
import 'package:movies_app/views/home.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState(){
    Future.delayed(const Duration(milliseconds: 3000), () {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
          const Home()), (Route<dynamic> route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            AppAssets.splashScreenImageLink,
            fit: BoxFit.cover,
            height: double.infinity,
          ),
           const Center(
             child:  CircularProgressIndicator(color: Colors.white,),
          ),
          Container(
            margin: const EdgeInsets.only(top: 200),
            height: 120,
            color: Colors.white.withOpacity(0.7),
            width: double.infinity,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  AppStrings.theMoviesApp,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontFamily: 'Lobster',
                      fontSize: 35,
                      fontWeight: FontWeight.bold,
                      color: AppColors.primary
                  ),
                ),
                const SizedBox(height: 10,),
                Text(
                  AppStrings.splashScreenSubtitle,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 18,
                    color: Colors.black87
                  ),
                ),
              ],
            )
          ),
        ],

      )
    );
  }
}

