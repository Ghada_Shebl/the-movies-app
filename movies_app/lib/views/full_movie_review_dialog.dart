import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movies_app/models/movie_review.dart';
import 'package:movies_app/resources/strings.dart';

class FullMovieReviewDialog extends StatefulWidget {
  final MovieReview movieReview;
  const FullMovieReviewDialog({Key? key, required this.movieReview})
      : super(key: key);

  @override
  State<FullMovieReviewDialog> createState() => _FullMovieReviewDialogState();
}

class _FullMovieReviewDialogState extends State<FullMovieReviewDialog> {
  late MovieReview movieReview;
  @override
  void initState() {
    movieReview = widget.movieReview;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: Container(
        padding: const EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                movieReview.author.name.isEmpty?AppStrings.unknown:movieReview.author.name,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 16,  color: Colors.black87),
              ),
              Container(
                margin: const EdgeInsets.only(top: 5, bottom: 5),
                child:  RatingBarIndicator(
                    rating: movieReview.author.rating??0 % 5,
                    itemSize: 20,
                    itemBuilder: (context, index) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    )),
              ),
              Text(
                movieReview.content,
                style: const TextStyle(
                  color: Colors.black87,
                  fontSize: 15,
                  height: 1.6
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
