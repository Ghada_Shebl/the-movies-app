import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:movies_app/bloc/movies/movie_bloc.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/views/movie_details.dart';

class MovieItem extends StatefulWidget {
  final Movie movie;
  const MovieItem({Key? key, required this.movie}) : super(key: key);

  @override
  State<MovieItem> createState() => _MovieItemState();
}

class _MovieItemState extends State<MovieItem> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => MoviesDetails(movieId: widget.movie.id)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: CachedNetworkImage(
              imageUrl:
              AppConstants.posterPathBaseURL + widget.movie.posterPath,
              fit: BoxFit.cover,
              height: 200,
            ),
          ),
          Container(
              margin: const EdgeInsets.only(top: 10, bottom: 20),
              child: Text(
                widget.movie.title,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Colors.black87
                ),
              )
          )],
      ),
    );
  }
}
