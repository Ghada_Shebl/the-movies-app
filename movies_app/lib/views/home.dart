import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movies_app/bloc/movies/movie_bloc.dart';
import 'package:movies_app/bloc/movies/movie_event.dart';
import 'package:movies_app/bloc/movies/movie_state.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/resources/constants.dart';
import 'package:movies_app/resources/strings.dart';
import 'package:movies_app/views/movie_item.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin{
  List <String> movieTabs = [AppStrings.upcoming,AppStrings.popular,AppStrings.topRated];

  List <Movie> movies = List.empty(growable: true);
  late TabController moviesTabController;

  void _getData(){
    context
        .read<MovieBloc>()
        .add(MoviesListLaunched(moviesTabController.index));
  }

  @override
  void initState() {
    super.initState();
    moviesTabController = TabController(length: AppConstants.moviesTabsCount, vsync: this)
      ..addListener(() {
        _getData();
      });
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: AppConstants.moviesTabsCount,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              AppStrings.browseMovies,
            ),
            bottom: TabBar(
                controller: moviesTabController,
                onTap: (int clickedTabID){
                  _getData();
                },
                tabs: _buildTabBarItems()
            ),
          ),
          body: BlocBuilder<MovieBloc,MovieState>(
            buildWhen: (_, state) => _isEligible(state),
            builder: (context,state){
              if (state is MoviesListIsLoading){
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is MoviesListIs){
                movies = state.movies;
                return TabBarView(
                  controller: moviesTabController,
                  children: [_buildMoviesGrid(movies), _buildMoviesGrid(movies), _buildMoviesGrid(movies)],
                );
              }
              if (state is MoviesListFailedToLoad){
                return _buildLoadingFailedWidget();
              }
              return Container();
            },
          )
      )
    );
  }

  Widget _buildLoadingFailedWidget(){
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
              AppStrings.failedToLoadMoviesException,
            style: const TextStyle(
              fontSize: 16,
              color: Colors.black87
            ),
          ),
          const SizedBox(height: 10,),
          ElevatedButton(
            onPressed: (){
              _getData();
            },
            child: const Text(
                AppStrings.retry
            ),
          )
        ],
      ),
    );
  }

  List<Widget> _buildTabBarItems() {
    List <Widget> widgets = List.empty(growable: true);
    for (int i = 0; i < AppConstants.moviesTabsCount; i++){
      widgets.add(
          Container(
            padding: const EdgeInsets.only(bottom: 10, top: 10),
            child: Text(
              movieTabs[i],
              style: const TextStyle(
                fontSize: 16,
              ),
            ),
          )
      );
    }
    return widgets;
  }

  Widget _buildMoviesGrid(List<Movie>movies) {
    return RefreshIndicator(
      onRefresh: ()async{
        _getData();
      },
      child: GridView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.all(10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          childAspectRatio: 0.6,
        ),
        itemCount: movies.length,
        itemBuilder: (_, int index) {
          return MovieItem(
            movie: movies[index],
          );
        },
      )
    );
  }

  bool _isEligible(MovieState state) {
    return state is MoviesListIsLoading ||
        state is MoviesListIs ||
        state is MoviesListFailedToLoad;
  }

}
