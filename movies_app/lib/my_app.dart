import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:movies_app/bloc/movies/movie_bloc.dart';
import 'package:movies_app/bloc/reviews/review_bloc.dart';
import 'package:movies_app/views/home.dart';
import 'package:movies_app/resources/themes.dart';
import 'package:movies_app/views/splash_screen.dart';


class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {

  void _initData(){
    GetIt.instance.registerLazySingleton<MovieBloc>(() => MovieBloc());
    GetIt.instance.registerLazySingleton<ReviewBloc>(() => ReviewBloc());
  }

  @override
  void initState() {
    _initData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
     return MultiBlocProvider(
       providers: [
         BlocProvider<MovieBloc>(create: (context) => GetIt.I<MovieBloc>()),
         BlocProvider<ReviewBloc>(create: (context) => GetIt.I<ReviewBloc>()),
       ],
      child: MaterialApp(
        home: const SplashScreen(),
        theme: AppThemes.appTheme,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
