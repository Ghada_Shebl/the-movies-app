class AppConstants {
  static int defaultMoviesTabID = 0;
  static int moviesTabsCount = 3;
  static String results = "results";
  static String posterPathBaseURL = "https://image.tmdb.org/t/p/w300";
  static String moviesBaseURL = "https://api.themoviedb.org/3/movie/";
  static String apiKey = "?api_key=0eb0e2605cc191f5793d628e380d9b6d&language=en-US&page=1";
  static String topRatedMoviesAPI = "top_rated";
  static String popularMoviesAPI = "popular";
  static String upcomingMoviesAPI = "upcoming";
  static String reviewsAPI = "/reviews";
  static String slash = "/";

}

class AppAssets{
  static String splashScreenImageLink = "assets/images/splash_background.jpg";
}

