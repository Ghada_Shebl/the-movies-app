class AppStrings {
  // UI Strings
  static String theMoviesApp = "The Movies App";
  static String splashScreenSubtitle = "I'm gonna make him an offer he can't refuse";
  static String movieDetails = "Movie Details";
  static String browseMovies = "Browse Movies";
  static String plot = "Plot";
  static String releaseDate = "Release Date: ";
  static String topRated = "Top Rated";
  static String upcoming = "Upcoming";
  static String popular = "Popular";
  static String failedToLoadMoviesException = "Failed to load movies";
  static String reviews = "Reviews";
  static const String unknown = "Unknown";
  static const String retry = "Try Again";

}