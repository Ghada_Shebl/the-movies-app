import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xFF5C6BC0);
  static const white = Color(0xFFffffff);
}